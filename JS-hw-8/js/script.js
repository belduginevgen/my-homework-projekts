// Обработчик событий это функция, которая позволяет совершать определенные взаимодействия с узлами Дом-дерева. Это могут быть клики клавишами мыши, нажатия кнопол, фокус на элементах и.д.



const priceInput = document.querySelector('.js-priceInput');
const priceLine = document.querySelector('.js-priceLine');
const priceInfo = document.querySelector('.js-priceInfo');
const errorInfo = document.querySelector('.js-errorInfo');
const xButton = document.querySelector('.js-xButton');

const getPrice = function() {
    priceInput.addEventListener('focus', function() {
        priceInput.style.cssText = `border: solid 2px green;`;
        errorInfo.style.opacity = '0';
    });

    priceInput.addEventListener('blur', function() {
        if (priceInput.value >= 0) {
            priceLine.innerHTML = `Текущая цена: ${priceInput.value}`;
            priceInput.style.color = 'green';
            priceInfo.style.opacity = '1';
        } else {
            priceInput.style.cssText = `border: solid 2px red;`;
            errorInfo.style.opacity = '1';
        }
    })

    xButton.addEventListener('click', function() {
        priceInfo.style.opacity = '0';
        priceInput.style.cssText = `solid 1px rgb(185, 185, 185);`;
        priceInput.value = '';
    })
}

getPrice();