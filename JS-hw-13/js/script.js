const changeThemeBtn = document.querySelector('.js-change-them-btn');
const head = document.querySelector('head');
const link = head.lastElementChild;

const changeTheme = function () {
    if (!link.hasAttribute('href')) {
        link.setAttribute('href', 'css/dark-theme.css');
        localStorage.setItem('them color', 'dark theme');
    } else {
        link.removeAttribute('href');
        localStorage.setItem('them color', 'white theme');
    }
}

changeThemeBtn.addEventListener('click', changeTheme);

if (localStorage.getItem('them color') === 'dark theme') {
    link.setAttribute('href', 'css/dark-theme.css');
} else if (localStorage.getItem('them color') === 'white theme') {
    link.removeAttribute('href');
}