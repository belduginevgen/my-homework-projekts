// Метод forEach перебирает элементы массива и выполняет функцию к каждому из них.




const array = [1, 2, 3, 'Mersedes', 'BMW', 'Audi', true, false];

//  способ 1
function filterBy(arr, dataType) {
    arr = arr.filter(function(element) {
        return typeof element !== dataType;
    });
    return arr;

}

const newArray = filterBy(array, 'string');

console.log(newArray);

// способ 2
function filterBy2(arr, dataType) {
    let arr2 = [];
    arr.forEach(element => {
        if (typeof element !== dataType) {
            arr2.push(element);
        }
    });

    return arr2;
}

const newArray2 = filterBy2(array, 'boolean');

console.log(newArray2);