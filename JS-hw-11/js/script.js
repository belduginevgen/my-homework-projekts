/* Часто для инпута клавиатуных событий не достаточно, по этому для инпутов есть 
свое событие, которое работает при любых изменениях значения. Также оно работает
при вставке скопированного содержания и диктовки на мобилках. */

const changeKey = function(event) {
    const key = event.code;
    const buttons = document.querySelectorAll('.js-button');
    for (const button of buttons) {
        button.classList.remove('active');
        const buttonId = button.dataset.keyCode;
        if (key === buttonId) {
            button.classList.add('active');
        }
    }
}

document.addEventListener('keyup', changeKey);