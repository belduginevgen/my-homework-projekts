const passwordForm = document.querySelector('.js-password-form');

const showPassword = function (event) {
    const iconTargetHide = event.target.closest('.js-eye-icon');
    if (iconTargetHide === null) return;
    const label = iconTargetHide.parentElement;
    const input = label.querySelector('.js-password-input');
    const iconShow = label.querySelector('.no-active');
    
    if (iconTargetHide.classList.contains('active')) {
        iconTargetHide.classList.toggle('active');
        iconTargetHide.classList.toggle('no-active');
        iconShow.classList.toggle('no-active');
        iconShow.classList.toggle('active');
    }

    if (input.getAttribute('type') === 'password') {
        input.setAttribute('type', 'text');
    } else {
        input.setAttribute('type', 'password');
    }  

}

passwordForm.addEventListener('click', showPassword);

const button = document.querySelector('.js-btn');

const passwordValidation = function (event) {
    event.preventDefault();

    const enterInput = passwordForm.querySelector('.js-enter-input');
    const confirmInput = passwordForm.querySelector('.js-confirm-input');
    const errorInfo = passwordForm.querySelector('.js-error-text')
    
    if (enterInput.value === '') {
        errorInfo.innerHTML = 'Нужно ввести одинаковые значения';
    } else if (enterInput.value === confirmInput.value) {
        alert('You are welcome');
        errorInfo.innerHTML = '';
    } else {
        errorInfo.innerHTML = 'Нужно ввести одинаковые значения';
    }
}

button.addEventListener('click', passwordValidation);