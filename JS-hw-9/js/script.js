const content = document.querySelector('.centered-content');
const heroNameTab = document.querySelectorAll('.js-tab');

const showDescription = function(event) {
    const tab = event.target.closest('.js-tab');
    
    for (const heroName of heroNameTab) {
        heroName.classList.remove('active');
    }

    tab.classList.add('active');

    const heroId = tab.dataset.hero;

    const descriptions = document.querySelectorAll('.js-description');

    for (const description of descriptions) {
        const descriptionId = description.dataset.description
        description.classList.remove('active');
        if (heroId === descriptionId) {
            description.classList.add('active');
        }
        
    }
    
}

content.addEventListener('click', showDescription);
