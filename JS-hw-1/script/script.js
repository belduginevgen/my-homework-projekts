// Теория.
// 1. У var отличается область видимости, в отличии от let и const. 
// Также с var можно обьявлять переменную с одинаковым названием повторно, 
// что недопустимо с let и const. У let и const область видимости одинаковая,
// но в переменных, обьявленных через let, как и у var, может меняться значение,
// а у const значение неизменно.
// 2. var считается устаревшим способом обьявления переменных, по этому сейчай
// этот способ не используется.



let userName = prompt('What is your name?');

if (userName == false) {
    userName = prompt('What is your name? Please enter correctly!');
}

let ageOfUser = Number(prompt('How old are you?'));

if (Number.isNaN(ageOfUser) == true) {
    alert('Please, enter only numbers')
    ageOfUser = Number(prompt('How old are you?'));
}

let isReady;

if (ageOfUser < 18) {
    alert('You are not allowed to visit this website.');
} else if (ageOfUser < 22) {
    isReady = confirm('Are you sure you want to continue?');
    
    if (isReady == false) {
        alert('You are not allowed to visit this website.');
    } else {
        alert(`Welcome, ${userName}`);
    }
} else {
    alert(`Welcome, ${userName}`);
}