// Цыклы нужны для определенных повторений, чтобы не писать лишний код. 


const number = +prompt('Enter your number?');

for (let i = 0; i <= number; i++) {
    if (i % 5 === 0) {
        console.log(i);
    }
}

if (number < 0 || isNaN(number) == true) {
    console.log('Sorry, no numbers');
}