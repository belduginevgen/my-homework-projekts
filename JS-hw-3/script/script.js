// 1. Фуекции нужны для того, чтобы избежать дублирование кода, к примеру, если 
// нам нужно повторно вывести сообщение или совершить какую-небудь одинаковую
// операцию.
// 2. Функция может быть и без аргументов, но они нужны для того, чтобы можно 
// было поставить на место аргументов числа или переменные, например, при
// выполнении математической операции. Также можно с помощью аргументов выводить
// похожий текст, но заменить слова, как сделано  функции getNumber.



function getNumber(iteration) {
    let num = +prompt(`Enter your ${iteration} number, please`);
    if (isNaN(num)) {
        num = +prompt(`Oops, it's not a number. Plese, enter your ${iteration} number, one more time`);
    }

    return num;
}

let firstNumber = getNumber('first');

let operator = prompt('Now, enter the math operation you want to perform');

let secondNumber = +getNumber('second');

function getResult (firstNum, secondNum, operat) {
    switch (operat) {
        case "+":
            return firstNum + secondNum;
        case "-":
            return firstNum - secondNum;
        case "*":
            return firstNum * secondNum;
        case "/":
            return firstNum / secondNum;
        default:
            alert('It is not math operation');
    }
    
}

console.log(getResult(firstNumber, secondNumber, operator));