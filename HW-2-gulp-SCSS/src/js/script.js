const btnMenu = document.querySelector('.header-btn'); 
const closeIcon = document.querySelector('.header-btn__x-btn');
const menu = document.querySelector('.header-menu');



const showMenu = function () {
    closeIcon.classList.toggle('header-btn__x-btn--disabled') 
    menu.classList.toggle('header-menu--disabled')
}

btnMenu.addEventListener('click', showMenu);