$(document).ready(function() {
    $(".scroll-link").click(function() {
       $("html, body").animate({
          scrollTop: $($(this).attr("href")).offset().top + "px"
       }, {
          duration: 500,
          easing: "swing"
       });
       return false;
    });
 });

$(function () {
    let element = $(".scroll-btn"), display;
    $(window).scroll(function () {
        display = $(this).scrollTop() >= 400;
        if(display){
            element.show();
        }else{
            element.hide();
        }
    });
});

$(".show-news-btn").click(function() {
    $(".news-hide").slideToggle("slow");
  });