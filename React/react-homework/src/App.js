import React, { useEffect, useState } from 'react'
import './App.scss'
import modalContent from './components/modal/modal-content'
import Header from './Header'
import { Route, Routes } from 'react-router'
import { Basket } from './components/Basket/Basket'
import { Favorite } from './components/Favorite/Favorite'
import Main from './components/Main/Main'

const App = () => {
    const [modalToShow, setShownModal] = useState('hide')
    const [modalState, setModalState] = useState({})
    const [allProducts, setAllProducts] = useState([])
    const [favoriteProducts, setFavoriteProducts] = useState([])
    const [productsToBasket, setProductsToBasket] = useState([])
    const [cardId, setCardId] = useState(0)


    useEffect(() => {
        fetch('./products.json')
            .then(response => response.json())
            .then(products => {
                setAllProducts(products)
            })
    }, [])

    useEffect(() => {
        const savedFavoriteProducts = JSON.parse(localStorage.getItem('favorite-products'))

        if (savedFavoriteProducts === null) {
            return false
        } else {
            setFavoriteProducts(savedFavoriteProducts)

        }

        const addedToBasketProducts = JSON.parse(localStorage.getItem('basket-products'))

        if (addedToBasketProducts === null) {
            return false
        } else {
            setProductsToBasket(addedToBasketProducts)
        }
    }, [])

    const addToFavorite = (obj) => {
        if (!favoriteProducts.includes(obj)) {
            setFavoriteProducts(prev => [...prev, obj])
        } else {
            const newProducts = favoriteProducts.filter(product => product.setNumber !== obj.setNumber)
            setFavoriteProducts(newProducts)
        }
    }

    const addToBasket = () => {
        const product = allProducts.find(product => product.setNumber === cardId)

        if (!productsToBasket.includes(product)) {
            setProductsToBasket(prev => [...prev, product])
        }
        closeModal()
    }

    const removeFromBasket = () => {
        const newProducts = productsToBasket.filter(product => product.setNumber !== cardId)
        setProductsToBasket(newProducts)
        closeModal()
    }

    useEffect(() => {
        putToStorage('favorite-products', favoriteProducts)
    }, [favoriteProducts])

    useEffect(() => {
        putToStorage('basket-products', productsToBasket)
    }, [productsToBasket])


    const putToStorage = (key, state) => {
        localStorage.setItem(key, JSON.stringify(state))
    }

    const openModal = (e) => {
        const btnId = e.target.closest('.card__icon-button').dataset.id;
        const cardId = e.target.closest('.card').dataset.cardId;

        const modalData = modalContent.find(item => item.id === +btnId);

        setModalState(modalData)
        setShownModal('show')
        setCardId(cardId)
    }

    const closeModal = () => {
        setShownModal('hide')
    }

    return (
        <>
            <Header />
            <div className="content-wrapper">
                <Routes >
                    <Route path="/" element={
                        <Main
                            products={allProducts}
                            handleAddToFavorite={addToFavorite}
                            handleOpenModal={openModal}
                            modalState={modalState}
                            modalToShow={modalToShow}
                            closeModal={closeModal}
                            addToBasket={addToBasket}
                        />
                    }/>
                    <Route path="/favorite" element={
                        <Favorite  
                            products={favoriteProducts}
                            handleOpenModal={openModal}
                            handleAddToFavorite={addToFavorite}
                            modalState={modalState}
                            modalToShow={modalToShow}
                            closeModal={closeModal}
                            addToBasket={addToBasket}
                        />
                    }/>
                    <Route path="/basket" element={
                        <Basket 
                            products={productsToBasket}
                            handleOpenModal={openModal}
                            modalState={modalState}
                            modalToShow={modalToShow}
                            closeModal={closeModal}
                            removeFromBasket={removeFromBasket}
                        />
                    }/>
                </Routes>
            </div>
        </>
    )
}

export default App;
