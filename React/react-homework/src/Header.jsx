import { Link } from "react-router-dom"

const Header = () => {
     return (
          <div className="header">
               <img src="./img/logo/danelli-logo.png" alt="logo" className="header__logo" />
               <h1 className="header__title">.Danelli</h1>
               <div className="header__links-wrapper">
                    <Link className="header__link" to="/">MAIN</Link>
                    <Link className="header__link" to="/favorite">FAVORITE</Link>
                    <Link className="header__link" to="/basket">BASKET</Link>
               </div>
          </div>
     )
}

export default Header