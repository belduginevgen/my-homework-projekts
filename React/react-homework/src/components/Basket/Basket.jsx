import React from 'react'
import ProductsList from '../ProductsList/ProductsList'
import Button from '../button/Button'
import Modal from '../modal/Modal'

export const Basket = ({products, handleOpenModal, modalState, modalToShow, closeModal, removeFromBasket }) => {
     const { backgroundColor, title, text, btn1, btn2 } = modalState
     return (
          <div>
               <h2 className="content-wrapper__title">BASKET</h2>
               <ProductsList products={products} >
                    <Button
                         onClick={handleOpenModal}
                         id='1'
                         className='card__icon-button'
                         btnColor='#8397A7'
                    >
                         <i className="fa fa-times"></i>
                    </Button>
               </ProductsList>
               {
                    modalToShow === 'show' &&
                    <Modal
                         backgroundColor={backgroundColor}
                         onClick={closeModal}
                         header={title}
                         closeBtn={true}
                         text={text}
                    >
                         <Button onClick={removeFromBasket} className='button'>
                              {btn1}
                         </Button>
                         <Button onClick={closeModal} className='button'>
                              {btn2}
                         </Button>
                    </Modal>
               }
          </div>
     )
}
