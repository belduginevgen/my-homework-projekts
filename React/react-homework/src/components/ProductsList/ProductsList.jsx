import React from 'react'
import Card from '../Card/Card'
import PropTypes from 'prop-types'

const ProductsList = (props) => {
     const { products, handleAddToFavorite, children, showStarBtn } = props
     return (
          <div className="products-wrapper">
               {
                    products.map(product => {
                         return (
                              <Card
                                   key={+product.setNumber}
                                   cardId={product.setNumber}
                                   name={product.name}
                                   srcImg={product.image}
                                   color={product.color}
                                   price={product.price}
                                   size={product.sizes}
                                   starButton={showStarBtn}
                                   onAddToFavorite={() => { handleAddToFavorite(product) }}
                              >
                                   {children}
                              </Card>
                         )
                    })
               }
          </div>
     )
}

ProductsList.propTypes = {
     product: PropTypes.object,
     showStarBtn: PropTypes.bool,
}

export default ProductsList
