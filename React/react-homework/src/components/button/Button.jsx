import PropTypes from 'prop-types'

const Button = (props) => {
    
    const { id, className, onClick, btnColor, children } = props

    return (
        <>
        <button 
            data-id={id}
            className={className} 
            onClick={onClick} 
            style={{color: btnColor}}
        >{children}</button>
    </>
    )
}

Button.propTypes ={
    id: PropTypes.string,
    className: PropTypes.string,
    onClick: PropTypes.func,
    btnColor: PropTypes.string,
}

export default Button