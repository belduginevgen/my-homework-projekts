export const buttonsContent = [
    {
        btnid: 0,
        child: <i className="fa fa-star"></i>,
        className: 'card__icon-button',
        color: 'rgb(169, 169, 169)',
    },
    {
        btnid: 1,
        child: <i className="fa fa-shopping-cart card__basket"></i>,
        className: 'card__icon-button',
        color: 'rgb(65, 65, 121)',
    },
    {

    }
]