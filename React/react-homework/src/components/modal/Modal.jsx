import React from "react"
import PropTypes from 'prop-types'

const Modal = (props) => {
    const { onClick, backgroundColor, header, closeBtn, text, children } = props

    return (
        <div onClick={onClick} className="modal">
            <div onClick={e => e.stopPropagation()} className="modal__wrapper" style={{ background: backgroundColor }}>
                <h2 className="modal__title">{header}</h2>
                {
                    closeBtn === true &&
                    <button onClick={onClick} className="modal__x-btn">
                        <img className="modal__x-icon" src="./x-btn.png" alt="" />
                    </button>
                }
                <p className="modal__text">{text}</p>
                <div className="modal__buttons-wrapper">
                    {children}
                </div>
            </div>
        </div>
    )
}

Modal.propTypes = {
    onClick: PropTypes.func,
    backgroundColor: PropTypes.string,
    header: PropTypes.string,
    closeBtn: PropTypes.bool,
    text: PropTypes.string,
}

export default Modal