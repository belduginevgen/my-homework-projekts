const modalContent = [
    {
        id: 1,
        title: 'Do you want to remove this product?',
        text: 'If you remove this product from basket, it can be added from main and favorite pages.',
        btn1: 'Yes',
        btn2: 'No',
        backgroundColor: 'rgb(140, 80, 80)',
    },
    {
        id: 2,
        title: 'Do you want to add this product to basket?',
        text: 'Are you sure you want to add this item to the basket? After addition you can choose more items.',
        btn1: 'Yes',
        btn2: 'No',
        backgroundColor: '#8397A7'    
    }
]

export default modalContent;