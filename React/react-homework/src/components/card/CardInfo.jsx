import PropTypes from 'prop-types';

const CardInfo = (props) => {
    
    const { color, price, size } = props

    return (
        <div className="card__info-wrapper">
        <div className="card__color-wrapper">
            <span className="card__item">Color: </span>
            <div style={{background: color}} className="card__circle-color"></div>
        </div>
        <span className="card__item">Price: {price}</span>
        <span className="card__item">Sizes: {size}</span>
    </div>
    )
}

CardInfo.propTypes = {
    color: PropTypes.string,
    price: PropTypes.string,
    size: PropTypes.string,
}

export default CardInfo