export const productCatalogue = [
    {
        name: 'Monks "Cambridge"',
        image: './img/mans-shoes/monks-cambridge.jpg',
        setNumber: '01',
        price: '120 $',
        color: 'brown',
        sizes: '41 - 45'
    },
    {
        name: 'Brogues "York"',
        image: './img/mans-shoes/brogues-york.jpg',
        setNumber: '02',
        price: '115 $',
        color: 'red',
        sizes: '39 - 44'
    },
    {
        name: 'Oxfords "Lancaster"',
        image: './img/mans-shoes/oxfords-lancaster.jpg',
        setNumber: '03',
        price: '135 $',
        color: 'black',
        sizes: '40 - 46'
    },
    {
        name: 'Brogues "London"',
        image: './img/mans-shoes/brogues-london.jpg',
        setNumber: '04',
        price: '100 $',
        color: 'blue',
        sizes: '39 - 46'
    },
    {
        name: 'Monks "Newcastle"',
        image: './img/mans-shoes/monks-newcastle.jpg',
        setNumber: '05',
        price: '110 $',
        color: 'black',
        sizes: '41 - 44'
    },
    {
        name: 'Jodhpurs "Henry"',
        image: './img/mans-shoes/jodhpurs-henry.jpg',
        setNumber: '06',
        price: '145 $',
        color: 'red',
        sizes: '41 - 46'
    },
    {
        name: 'Loafers "Manchester"',
        image: './img/mans-shoes/loafers-manchester.jpg',
        setNumber: '07',
        price: '120 $',
        color: 'brown',
        sizes: '41 - 44'
    },
    {
        name: 'Derby "Whiskey"',
        image: './img/mans-shoes/derby-whiskey.jpg',
        setNumber: '08',
        price: '105 $',
        color: 'black',
        sizes: '39 - 44'
    },
    {
        name: 'Brogues "Croydon"',
        image: './img/mans-shoes/brogues-croydon.jpg',
        setNumber: '09',
        price: '120 $',
        color: 'brown',
        sizes: '41 - 45'
    },
    {
        name: 'Chelsey "Richard"',
        image: './img/mans-shoes/chelsey-richard.jpg',
        setNumber: '10',
        price: '150 $',
        color: 'black',
        sizes: '41 - 4'
    }
]