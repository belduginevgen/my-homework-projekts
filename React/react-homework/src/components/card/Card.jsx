import React, { useState } from 'react'
import CardInfo from './CardInfo'
import Button from '../button/Button'
import PropTypes from 'prop-types';

const Card = (props) => {

    const [starAddedState, setStarState] = useState(false)

    const { cardId, name, srcImg, color, price, size, children, starButton } = props

    const onStarClick = () => {
        props.onAddToFavorite()
        setStarState(!starAddedState)
    }

    return (
        <div className="card" data-card-id={cardId}>
            <h3 className="card__title">{name}</h3>
            <img className="card__img" src={srcImg} alt="shoes" />
            <CardInfo color={color} price={price} size={size} />
            <div className="card__buttons-wrapper">
                {
                    starButton &&
                    <Button
                        onClick={onStarClick}
                        className='card__icon-button'
                        btnColor={starAddedState ? 'rgb(255, 212, 103)' : 'rgb(211, 211, 211)'}
                    >
                        <i className="fa fa-star"></i>
                    </Button>
                }
                {children}
            </div>
        </div>
    )
}
Card.propTypes = {
    cardId: PropTypes.string,
    name: PropTypes.string,
    srcImg: PropTypes.string,
    starButton: PropTypes.bool,
    onStarClick: PropTypes.func,
}


export default Card