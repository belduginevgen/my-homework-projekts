import React from 'react'
import Button from '../button/Button'
import ProductsList from '../ProductsList/ProductsList'
import Modal from '../modal/Modal'

const Main = ({ products, handleAddToFavorite, handleOpenModal, modalState, modalToShow, closeModal, addToBasket }) => {
     const { backgroundColor, title, text, btn1, btn2 } = modalState

     return (
          <>
               <h2 className="content-wrapper__title">MAIN</h2>
               <ProductsList products={products} handleAddToFavorite={handleAddToFavorite} showStarBtn={true}>
                    <Button
                         onClick={handleOpenModal}
                         id='2'
                         className='card__icon-button'
                         btnColor='#8397A7'
                    >
                         <i className="fa fa-shopping-cart card__basket"></i>
                    </Button>
               </ProductsList>
               {
                    modalToShow === 'show' &&
                    <Modal
                         backgroundColor={backgroundColor}
                         onClick={closeModal}
                         header={title}
                         closeBtn={true}
                         text={text}
                    >
                         <Button onClick={addToBasket} className='button'>
                              {btn1}
                         </Button>
                         <Button onClick={closeModal} className='button'>
                              {btn2}
                         </Button>
                    </Modal>
               }
          </>
     )
}

export default Main
