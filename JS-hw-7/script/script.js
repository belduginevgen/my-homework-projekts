// Я понимаю DOM по своему. Для меня DOM это своеобразная копия кода html, которую браузер создает себе для работы и изменения Web-страниц. Тоесть он берет эту вот копию и дает её JS коду, который в свою очередь выполняет уже свою работу. Выполняет интерактивные действия, добавляет элементы или удаляет, творит с ними, что захочет программист и т.д. И все это он делает в DOM, а наш html документ лежит себе, отдыхает и в нем ничего не меняется)))




const countries = ['Ukraine', 'Spain', 'Ireland', 'Argentina', 'Norway'];



const showListBtn = document.getElementById('showList');

const countriesList = document.getElementById('countriesList');


const createList = function () {
    const newCountriesListsItems = countries.map(function(country) {
        country = `<li>${country}</li>`;
        return country;
    });

    newCountriesListsItems.forEach(element => {
        countriesList.insertAdjacentHTML('beforeend', element);
    });
}

showListBtn.addEventListener('click', createList);