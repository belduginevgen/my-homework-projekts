const serviceSection = document.querySelector('.js-service');
const serviceListItems = document.querySelectorAll('.js-service-list-item');

const showDescription = function(event) {
    const itemTarget = event.target.closest('.js-service-list-item');

    for (const listItem of serviceListItems) {
        listItem.classList.remove('active');
    }

    itemTarget.classList.add('active');

    const itemId = itemTarget.dataset.serviceName;

    const serviceDescriptions = document.querySelectorAll('.js-service-container');

    for (const description of serviceDescriptions) {
        const descriptionId = description.dataset.serviceInfo;
        description.classList.remove('active');
        if (itemId === descriptionId) {
            description.classList.add('active');
        } 
    }
}

serviceSection.addEventListener('click', showDescription);

const workSection = document.querySelector('.js-work-list');
const workListItems = document.querySelectorAll('.js-work-list-item');

const showImages = function (event) {
    const itemTarget = event.target.closest('.js-work-list-item');

    for (const listItem of workListItems) {
        listItem.classList.remove('active');
    }

    itemTarget.classList.add('active');

    const itemId = itemTarget.dataset.workItem;

    const images = document.querySelectorAll('.work-img');

    for (const image of images) {
        let imageId = image.dataset.workName;
        
        if (itemId === 'all') {
            imageId = image.dataset.workAll;
        }
        
        image.classList.remove('active');
        if (itemId === imageId) {
            image.classList.add('active');
        } 
    }
}

workSection.addEventListener('click', showImages);

const loadMoreButton = document.querySelector('.js-load-more-btn');

const loadImages = function () {
    const images = document.querySelectorAll('.work-img');

    for (const image of images) {
        let imageId = image.dataset.workName;

        if (imageId === 'load-all') {
            image.setAttribute('data-work-all', 'all');
            image.classList.add('active');
        }

        loadMoreButton.classList.remove('active');
    }
}

loadMoreButton.addEventListener('click', loadImages);

const rewiewNavigator = document.querySelector('.rewiewers-navigator');
const rewiewTargets = document.querySelectorAll('.js-rew-target');

const showRewiew = function(event) {
    let rewTarget = event.target.closest('.js-rew-target');
    
    if (rewTarget.classList.contains('right-btn')) {
        let oldActiveTarget = rewiewNavigator.querySelector('.active');
        rewTarget = oldActiveTarget.nextElementSibling;
        if (rewTarget.classList.contains('right-btn')) {
            rewTarget = rewiewTargets[1];
        }
    }

    if (rewTarget.classList.contains('left-btn')) {
        let oldActiveTarget = rewiewNavigator.querySelector('.active');
        rewTarget = oldActiveTarget.previousElementSibling;
        if (rewTarget.classList.contains('left-btn')) {
            rewTarget = rewiewTargets[4];
        }
    }

    for (const target of rewiewTargets) {
        target.classList.remove('active');
    }

    rewTarget.classList.add('active');

    const targetId = rewTarget.dataset.rewTarget;
    const rewiewes = document.querySelectorAll('.js-rewiew');

    for (const rewiew of rewiewes) {
        const rewiewId = rewiew.dataset.rewName;
        rewiew.classList.remove('active');
        if (targetId === rewiewId) {
            rewiew.classList.add('active');
        } 
    }
}

rewiewNavigator.addEventListener('click', showRewiew);