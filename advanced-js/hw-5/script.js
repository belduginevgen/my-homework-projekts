const fetchJSON = (url, initRequest) =>
  fetch(url, initRequest)
    .then(response => response.json());


const request = async () => {
    const { ip } = await fetchJSON('https://api.ipify.org/?format=json')

    const response = await fetchJSON(`http://ip-api.com/json/${ip}?fields=continent,country,region,regionName,city`)

    const responseData = await response;

    const { continent, country, region, regionName, city } = responseData;

    const arrData = [continent, country, region, regionName, city ];

    showInfo(arrData);
}

const showInfo = (arrToShow) => {
    const infoList = document.querySelector('.info-list');
    arrToShow.forEach(element => {
        const listItem = document.createElement('li');
        listItem.innerHTML = element

        infoList.appendChild(listItem);
    })
}

const button = document.querySelector('.button');
button.addEventListener('click', request);

