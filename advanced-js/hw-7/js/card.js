class Card {
    constructor(name, email, title, body, id) {
        this.name = name;
        this.email = email;
        this.title = title;
        this.body = body;
        this.id = id;
        this.mainContainer = document.querySelector('#root');
    }

    createPost() {
        const cardWrapper = document.createElement('div');
        cardWrapper.classList.add('card-wrapper');
        cardWrapper.id = this.id;
        
        cardWrapper.innerHTML = 
        `<span class="user-name">${this.name}</span>
        <span class="email">${this.email}</span>
        <button class="post-delete-button">х</button>
        <h3 class="post-title">${this.title}</h3>
        <p class="post-text">${this.body}</p>`;
        
        this.mainContainer.appendChild(cardWrapper);

    }

    deletePost(id) {
        const deleteChosenPost = async function(event) {
            const deletePostBtn = event.target.closest('.post-delete-button');
            
            if (deletePostBtn === null) return;
           
            const cardWrapper = deletePostBtn.parentElement;
            const postId = cardWrapper.id;
            
            if (+postId === id) {
                await fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
                    method: 'DELETE'
                });
                cardWrapper.remove()
            }
        }
        this.mainContainer.addEventListener('click', deleteChosenPost);
    }
}