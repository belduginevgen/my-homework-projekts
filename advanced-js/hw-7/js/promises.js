const getUsersPromise = fetch('https://ajax.test-danit.com/api/json/users')
        .then((response) => {
            return response.json();
});

const getPostsPromise = fetch('https://ajax.test-danit.com/api/json/posts')
        .then((response) => {
            return response.json();
});

const promise = Promise.all([getUsersPromise, getPostsPromise]);

promise.then(([users, allPosts]) => {
    const usersWithPosts = users.map(user => {
        let postsWithSameId = [];
        allPosts.forEach(post => {
            if (post.userId === user.id) {
                postsWithSameId.push(post);
            }
        })

        user.posts = postsWithSameId;
        return user;
    })

    usersWithPosts.forEach(({ name, email, posts }) => {
        posts.forEach(post => {
            post = new Card(name, email, post.title, post.body, post.id);
            post.createPost();

            const { id } = post;
            post.deletePost(id);
        })
    })
})