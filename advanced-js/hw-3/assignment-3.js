const user1 = {
    name: "John",
    years: 30
};

const { name, years: age, isAdmin = false} = user1;

console.log('Задание 3');
console.log(name, age, isAdmin);