const requestUrl = 'https://ajax.test-danit.com/api/swapi/films';
const moviesContainer = document.querySelector('.root');

fetch(requestUrl)
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    data.forEach(({ name, episodeId, openingCrawl, characters }) => {
      const movie = new Movie(name, episodeId, openingCrawl, characters);
      movie.render();
    });
});

class Movie {
  constructor(name, episodeId, openingCrawl, characters) {
    this.name = name;
    this.episodeId = episodeId;
    this.openingCrawl = openingCrawl;
    this.characters = characters;
  }

  render() {
    const movieWrapper = document.createElement('div');
    movieWrapper.innerHTML = 
    `<h2>STAR WARS</h2>
     <h3>Episode: ${this.episodeId} <br> ${this.name}</h3>
     <p>${this.openingCrawl}</p>`;
    moviesContainer.appendChild(movieWrapper);
    
    const charactersList = document.createElement('ul');
    charactersList.innerHTML = 'Characters :';

    this.characters.forEach(character => {
      fetch(character)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        movieWrapper.appendChild(charactersList);
        const listItem = document.createElement('li');
        charactersList.appendChild(listItem);
        listItem.innerHTML = data.name;
      })
    })
  }
}