const books = [
  { 
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70 
  }, 
  {
   author: "Скотт Бэккер",
   name: "Воин-пророк",
  }, 
  { 
    name: "Тысячекратная мысль",
    price: 70
  }, 
  { 
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  }, 
  {
   author: "Дарья Донцова",
   name: "Детектив на диете",
   price: 40
  },
  {
   author: "Дарья Донцова",
   name: "Дед Снегур и Морозочка",
  }
]

const listContainer = document.querySelector('#root')
const list = document.createElement('ul')
listContainer.appendChild(list)

class ValidationError extends Error {
  constructor(message) {
    super(message)
    this.name = 'ValidationError'
  }
}

books.forEach(function(book) {
  const listItem = document.createElement('li')

  try {
    if (!book.name) {
      throw new ValidationError('Нет названия книги(name)!')
    } else if (!book.author) {
      throw new ValidationError('Не указан автор(author)!')
    } else if (!book.price) {
      throw new ValidationError('Не указана цена(price)!')
    }
    listItem.innerHTML = `${book.author}  "${book.name}"  <br>  Цена  ${book.price}`
    list.appendChild(listItem)
  } catch (err) {
    if (err instanceof ValidationError) {
      console.log(err.message);
    }
  }
})