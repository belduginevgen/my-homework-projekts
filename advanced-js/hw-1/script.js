class Employee {
    constructor(name, age, salary) {
        this._name = name
        this._age = age
        this._salary = salary
    }

    get name() {
        return this._name
    }

    get age() {
        return this._age
    }

    get salary() {
        return this._salary
    }

    set name(newName) {
        this._name = newName
    }

    set age(newAge) {
        this._age = newAge
    }

    set salary(newSalary) {
        this._salary = newSalary
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary)
        this._lang = lang
    }
    get leng() {
        return this._lang
    }

    get salary() {
        return this._salary * 3
    }
}

const programmerEugene = new Programmer ('Eugene', 28, 20000, 'ru')

const programmerIvan = new Programmer ('Ivan', 23, 15000, 'ukr')

const programmerViktoriia = new Programmer ('Viktoriia', 30, 30000, 'eng')

console.log(programmerEugene);
console.log(programmerIvan);
console.log(programmerViktoriia);
