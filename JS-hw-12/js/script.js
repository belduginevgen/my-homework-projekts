/* 1. setTimeout запускает функцию один раз, через указанное количество времени, а setInterval повторяет функцию постоянно, через указанное время.
2. Да, сработает, не совсем мгновенно, после выполнения всего кода, например, если ниже в коде есть алерт, то он сработает, а потом мгновенно setTimeout с нулевым значением в секундах.
3. Чтобы остановить выполнение бесконечной функции. */



const slider = function () {
    let activeImg = document.querySelector('.active');
    let nextImg = activeImg.nextElementSibling;
    
    if (nextImg === null) {
        nextImg = document.querySelector('.js-images');
        nextImg.classList.add('active');
    }

    if (activeImg.classList.contains('active')) {
        activeImg.classList.remove('active');
        nextImg.classList.add('active');
    }
    
}

let interval = setInterval(slider, 3000);

const buttons = document.querySelector('.js-buttons');

const intervalNavigation = function(event) {
    const button = event.target.closest('.js-btn');
    if (!button.classList.contains('js-btn')) return;

    const startBtn = buttons.querySelector('.btn-start');
    const stopBtn = buttons.querySelector('.btn-stop');

    if (button.classList.contains('btn-stop')) {
        clearInterval(interval);
        button.disabled = true;
        startBtn.disabled = false;
    }

    if (button.classList.contains('btn-start')) {
        interval = setInterval(slider, 3000);
        button.disabled = true;
        stopBtn.disabled = false;
    }
}

buttons.addEventListener('click', intervalNavigation);

